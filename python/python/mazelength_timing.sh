#!/bin/bash
# maze_timing.sh

# $1 is input file?
# $2 is output file
# $3 is END size
echo usage is 1:Source Code, 2:Out file, 3: MAX_LENGTH, more 9, less 81

INCREMENT=1
INPUT_FILE=$1
OUTPUT_FILE=$2
MAX_LENGTH=$3

for path_length in `seq 9 $INCREMENT $MAX_LENGTH`
do
echo -n length $path_length " " >> $OUTPUT_FILE
	for i in `seq 1 10` #10 times
	do
	echo TIMING maze of size 9, length $path_length
    (/usr/bin/time -f "%e " python3 $INPUT_FILE 9 1 2 $path_length $i 3>&1 1>&2 2>&3-) | tr '\n' ' ' >> $OUTPUT_FILE
	#(/usr/bin/time -f "%e " python3 $INPUT_FILE 9 1 2 $path_length $i) 2> (tr '\n' ' ') >> $OUTPUT_FILE
	done
echo " " >> $OUTPUT_FILE #newline
done








