from monosat import *
import sys
import time
import random
import colorama
colorama.init()

if len(sys.argv) != 6 and len(sys.argv) != 5:
    print(
"""
usage: %s <grid_size> <entry_col> <exit_col> <min_path_length> <rand_seed>
- grid_size:       INT
- entry_col:       INT in [0, grid_size-1]
- exit_col:        INT in [0, grid_size-1]
- min_path_lenght: INT in [0, grid_size**2]
- rand_seed:       INT
""" % sys.argv[0])
    exit(1)

print("Colored Maze generation in MonoSAT with bitvectors & graph theories")

VERBOSE = 2 # verbosity level
GRID_SIZE = int(sys.argv[1])
ENTRY_COL = int(sys.argv[2])
EXIT_COL = int(sys.argv[3])
PATH_LENGTH = int(sys.argv[4])

assert( 0 <= ENTRY_COL and ENTRY_COL <= GRID_SIZE-1 )
assert( 0 <= EXIT_COL and EXIT_COL <= GRID_SIZE-1 )
assert( 0 <= PATH_LENGTH and PATH_LENGTH <= GRID_SIZE**2 ) 

# color wheel RED -> BLUE -> GREEN -> YELLOW
RED = 0 
GREEN = 1
BLUE = 2
YELLOW = 3


def verbose(s, level):
    """
    Prints s if current verbose level is >= to given level
    """
    if ( VERBOSE >= level ):
        print( s )


print( "GRID_SIZE: %d" % GRID_SIZE )

RAND_SEED = sys.argv[5]
#Monosat().init(" -verb=2 -no-decide-theories")
#import random
#seed = str(random.randint(1,500))
Monosat().init("-rnd-freq=0.1 -rnd-seed="+RAND_SEED+ "-no-decide-theories")

# the graph representing the terrain grid
graph = Graph()

# dictionary mapping integer coordinates to node objects (i,j): n
nodes = dict()

# dictionary mapping pairs of node objects  to edge variables (n1,n2): e
#edges = dict()

# dictionary associating a color each node n:bv
color = dict() 

def print_color_map(entry_col, exit_col):
    """
    Prints the current value of the bitvector variable c to stdout as an integer.
    Uses colors to indicate values of terrain material, base or treasure presence.
    :param c:
    :return:

    """
    from colorama import Fore, Back, Style

    for i in range( GRID_SIZE ):

        print( "\ni:%2d | " % i, end="" )

        for j in range( GRID_SIZE ):
            node = nodes[i,j]                   ##INITIALIZES the mapping or already init?? (Python..)
            # background color
            color_idx = color[node].value()
            bcolor = Back.RESET
            if color_idx == RED:
                bcolor = Back.RED
            elif color_idx == GREEN:
                bcolor = Back.GREEN
            elif color_idx == BLUE:
                bcolor = Back.BLUE
            else : # color_idx == YELLOW 
                bcolor = Back.YELLOW
            if i==0 and j == entry_col:
                print( Fore.BLACK + bcolor + "**" , end="" )
            elif i==GRID_SIZE-1 and j == exit_col:
                print( Fore.BLACK + bcolor + "**" , end="" )          
            else:
                print( Fore.BLACK + bcolor + "  " , end="" ) #Empty maze spot

        print( Style.RESET_ALL, end="" )


#1. 
verbose( "Initializing a node for every (i, j) pair.", 1)

for i in range( GRID_SIZE ):
    for j in range (GRID_SIZE):
        node = graph.addNode()
        nodes[i,j] = node

#2. Initialize color "assignments" as BVs FIRST
verbose( "Initializing color BV for every node first", 1)

for node in nodes.values():
    color[node] = BitVector(2)  #4 colors (How does this work in the background??)

#3. Assert edge color conditions!
verbose( "Initializing edges for valid node pairs.", 1)

for i in range( GRID_SIZE ):
    for j in range (GRID_SIZE):
        dirs = [(i,j+1), (i, j-1), (i+1, j), (i-1, j)]
        for d in dirs:
            if 0 <= d[0]< GRID_SIZE and 0 <= d[1]< GRID_SIZE: #Valid direction!
                currnode = nodes[(i,j)]                
                nextnode = nodes[d]
                edge = graph.addEdge(currnode, nextnode)
                #edges[(currnode, nextnode)] = edge
                #TODO: Questions - these were asserted INCORRECTLY initially!
                Assert( Not(color[currnode]==color[nextnode]) )
                AssertEq(edge,
                        Or(  And(color[currnode]==RED, color[nextnode]==GREEN),
                            And(color[currnode]==GREEN, color[nextnode]==BLUE),
                            And(color[currnode]==BLUE, color[nextnode]==YELLOW),
                            And(color[currnode]==YELLOW, color[nextnode]==RED)
                        ))
                #Other direction implicitly covered in backedge condition??

#4. Assert start reaches end, and LOWER BOUND shortest path length!
startnode = nodes[0, ENTRY_COL]
endnode = nodes[GRID_SIZE-1, EXIT_COL]

Assert(graph.reaches(startnode, endnode))
Assert(Not(graph.distance_leq(startnode, endnode, PATH_LENGTH) ))

#MISSED CONDITION: Start with a RED -- dramatically speeds up time, is it necessary?!
Assert(color[startnode] == RED)

#MISSED CONDITION: No other first row cell reaches last row cell
for i in range(GRID_SIZE):
    for j in range(GRID_SIZE):
        #if (not(i==ENTRY_COL and j==EXIT_COL)):
        if i != ENTRY_COL and j != EXIT_COL:
            Assert(Not(graph.reaches(nodes[0, i], nodes[GRID_SIZE-1, j])))

#5. Solve, and print out result!
result = Solve()
if result:
    print( "\ncolor" )
    print_color_map(ENTRY_COL, EXIT_COL)
    print( "\n" )
    sys.stdout.flush()
else:
    print("UNSAT")
    sys.stdout.flush()


'''
Some implemenation notes: / thoughts for self!
1. Python debugging issue:

    Careful w/ .keys(), .values() next time!
    nodes{}: position pair -> node (DON'T take the whole mapping!)
    color{}: node -> color BV

2. Why do we assert that the condition holds only if edge exists in graph?
    When will edge "not exist" in graph?

3. Directed vs. Undirected edges?!?

4. Can Google CJ qs 2 be solved w/ this generative technique?

'''
