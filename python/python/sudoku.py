from monosat import *
import sys
import time

#Function constructs board constraints
def constructConstraints(constraints):
    #constraints[i][j][num] is Var() for "constraints[i][j] is num"
    for i in range(SUDOKU_SIZE): 
        row = list()
        for j in range(SUDOKU_SIZE):
            cell = list()
            for num in range(SUDOKU_SIZE):
                cell.append(Var())
            row.append(cell)
        constraints.append(row)

    #Exactly one num for each cell
    for row in range(SUDOKU_SIZE):
        for col in range(SUDOKU_SIZE):          
            numlist = list()
            for num in range(SUDOKU_SIZE):
                numlist.append(constraints[row][col][num])
            exactlyOne(numlist)

    #Exactly one of each num in each row
    for num in range(SUDOKU_SIZE):
        for row in range(SUDOKU_SIZE):
            #Fix row, num            
            rowlist = list()
            for col in range(SUDOKU_SIZE):
                rowlist.append(constraints[row][col][num])
            exactlyOne(rowlist)

    #Exactly one of each num in each col
    for num in range(SUDOKU_SIZE):
        for col in range(SUDOKU_SIZE):
            #Fix col, num            
            collist = list()
            for row in range(SUDOKU_SIZE):
                collist.append(constraints[row][col][num])
            exactlyOne(collist)

    #Exactly one of each num in each block
    for num in range(SUDOKU_SIZE):
        for refrow in range(0,SUDOKU_SIZE,3):
            for refcol in range(0,SUDOKU_SIZE,3):
                #Fix block           
                blocklist = list()
                for row in range(refrow, refrow+3):
                    for col in range(refcol, refcol+3):
                        blocklist.append(constraints[row][col][num])
                exactlyOne(blocklist)


#Input: a list of variables in which we force exactly ONE true
def exactlyOne(varlist):
    assert(len(varlist) == 9)
    #OR: >= 1 True
    Assert(Or(varlist[0],varlist[1],varlist[2],varlist[3],varlist[4],varlist[5],varlist[6],varlist[7],varlist[8]))
    #pairwise NAND, <=1 True
    for i in range(len(varlist)):
        for j in range(i+1,len(varlist)): #TODO: was off by one!
            Assert(Not(And(varlist[i], varlist[j])))


SUDOKU_SIZE = 9;
print ("Input board: EACH line should look like: ###7#1593")

#1. Read input

board = [input() for i in range(SUDOKU_SIZE)]
#print(board)


#2. Establish Sudoku constraints in MonoSAT
print( "GRID_SIZE: 10" )

Monosat().init()

constraints = []
constructConstraints(constraints)

#Assert puzzle specific constraints
for row in range(SUDOKU_SIZE):
    for col in range(SUDOKU_SIZE):
        if(board[row][col]!='#'):
            num = int(board[row][col])
            Assert(constraints[row][col][num-1])
            #print(board[row][col])
            #print("Asserting %d, %d to %d" % (row,col,num))


#3. Print out solution if satisfiable
starttime = time.clock()
result = Solve()
if result:
    print("SAT\n")
    for row in range(SUDOKU_SIZE):
        for col in range(SUDOKU_SIZE):
            assert(True in constraints[row][col])
            for num in range(SUDOKU_SIZE):
                #print(str(constraints[row][col][num].value()))
                choose = constraints[row][col][num].value()
                if choose:                
                    print(num+1, end="")
        print()
    sys.stdout.flush()
    
else:
    print("UNSAT\n")
    sys.stdout.flush()

print("Time taken: %s" % str(time.clock() - starttime))
