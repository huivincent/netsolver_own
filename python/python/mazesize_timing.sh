#!/bin/bash
# maze_timing.sh

# $1 is input file?
# $2 is output file
# $3 is END size
echo usage is 1:Source Code, 2:Out file, 3: END_SIZE

INCREMENT=1
INPUT_FILE=$1
OUTPUT_FILE=$2
END_SIZE=$3
path_length=6

for size in `seq 3 $INCREMENT $END_SIZE`
do
echo -n Size $size " " >> $OUTPUT_FILE
	for i in `seq 1 10`
	do
	echo TIMING maze of size $size
	(/usr/bin/time -f "%e " python3 $INPUT_FILE $size 1 2 $path_length $i 3>&1 1>&2 2>&3-) | tr '\n' ' ' >> $OUTPUT_FILE
	done
echo " " >> $OUTPUT_FILE #newline
done








