from monosat import *
import sys
import time
import random
import colorama
colorama.init()

if len(sys.argv) != 7:
    print(
"""
usage: %s <grid_size> <entry_row> <exit_row> <entry_col> <exit_col> <min_path_length>
- grid_size:       INT
- entry_row:       INT in [0, grid_size-1]
- exit_row:        INT in [0, grid_size-1]
- entry_col:       INT in [0, grid_size-1]
- exit_col:        INT in [0, grid_size-1]
- min_path_length: INT in [0, grid_size**2]
""" % sys.argv[0])
    exit(1)

print("Colored Maze generation in MonoSAT with bitvectors & graph theories")

VERBOSE = 2 # verbosity level
GRID_SIZE = int(sys.argv[1])
ENTRY_ROW = int(sys.argv[2])
EXIT_ROW = int(sys.argv[3])
ENTRY_COL = int(sys.argv[4])
EXIT_COL = int(sys.argv[5])
PATH_LENGTH = int(sys.argv[6])

assert( 0 <= ENTRY_COL and ENTRY_COL <= GRID_SIZE-1 )
assert( 0 <= EXIT_COL and EXIT_COL <= GRID_SIZE-1 )
assert( 0 <= PATH_LENGTH and PATH_LENGTH <= GRID_SIZE**3 ) 

# color wheel RED -> BLUE -> GREEN -> YELLOW
RED = 0 
GREEN = 1
BLUE = 2
YELLOW = 3


def verbose(s, level):
    """
    Prints s if current verbose level is >= to given level
    """
    if ( VERBOSE >= level ):
        print( s )


print( "GRID_SIZE: %d" % GRID_SIZE )

Monosat().init()
#Monosat().init("-no-decide-theories")

# the graph representing the terrain grid
graph = Graph()

# dictionary mapping integer coordinates to node objects (i,j): n
nodes = dict()

# dictionary mapping pairs of node objects  to edge variables (n1,n2): e TODO:don't need
#edges = dict()

# dictionary associating a color each node n:bv
color = dict() 

def print_color_map(entry_row, entry_col, exit_row, exit_col):
    """
    Prints the current value of the bitvector variable c to stdout as an integer.
    Uses colors to indicate values of terrain material, base or treasure presence.
    :param c:
    :return:
    """
    from colorama import Fore, Back, Style

    for level in range( GRID_SIZE ):
        print ("level %2d map:" % level)

        for i in range( GRID_SIZE ):

            print( "\ni:%2d | " % i, end="" )

            for j in range( GRID_SIZE ):
                node = nodes[level,i,j]
                # background color
                color_idx = color[node].value()
                bcolor = Back.RESET
                if color_idx == RED:
                    bcolor = Back.RED
                elif color_idx == GREEN:
                    bcolor = Back.GREEN
                elif color_idx == BLUE:
                    bcolor = Back.BLUE
                else : # color_idx == YELLOW 
                    bcolor = Back.YELLOW
                if level==0 and i==entry_row and j == entry_col:
                    print( Fore.BLACK + bcolor + "**" , end="" )
                elif level==GRID_SIZE-1 and i==exit_row and j == exit_col:
                    print( Fore.BLACK + bcolor + "EE" , end="" )
                else:
                    print( Fore.BLACK + bcolor + "  " , end="" ) #Empty maze spot
            print( Style.RESET_ALL, end="" )
        print("\n")


#1. 
verbose( "Initializing a node for every (i, j) pair.", 1)

for l in range( GRID_SIZE ):
    for i in range( GRID_SIZE ):
        for j in range (GRID_SIZE):
            node = graph.addNode()
            nodes[(l,i,j)] = node

#2. Initialize color "assignments" as BVs FIRST
verbose( "Initializing color BV for every node first", 1)

for node in nodes.values():
    color[node] = BitVector(2)  #4 colors (How does this work in the background??)

#3. Assert edge color conditions!
verbose( "Initializing edges for valid node pairs.", 1)

for l in range( GRID_SIZE ):
    for i in range( GRID_SIZE ):
        for j in range (GRID_SIZE):
            dirs = [(l,i,j+1), (l,i, j-1), (l,i+1, j), (l,i-1, j),
                    (l+1,i,j), (l-1,i,j)]
            for d in dirs:
                if 0 <= d[0]< GRID_SIZE and 0 <= d[1]< GRID_SIZE and 0 <= d[2]< GRID_SIZE: #Valid!
                    currnode = nodes[(l,i,j)]                
                    nextnode = nodes[d]
                    edge = graph.addEdge(currnode, nextnode)
                    #TODO: Questions - these were asserted INCORRECTLY initially!
                    Assert( Not(color[currnode]==color[nextnode]) )
                    AssertEq(edge,
                            Or(  And(color[currnode]==RED, color[nextnode]==GREEN),
                                And(color[currnode]==GREEN, color[nextnode]==BLUE),
                                And(color[currnode]==BLUE, color[nextnode]==YELLOW),
                                And(color[currnode]==YELLOW, color[nextnode]==RED)
                            ))
                    #Other direction implicitly covered in backedge condition??

#4. Assert start reaches end, and LOWER BOUND shortest path length!
startnode = nodes[0,ENTRY_ROW, ENTRY_COL]
endnode = nodes[GRID_SIZE-1, EXIT_ROW, EXIT_COL]

Assert(graph.reaches(startnode, endnode))
Assert(Not(graph.distance_leq(startnode, endnode, PATH_LENGTH) ))

#MISSED CONDITION: Start with a RED -- dramatically speeds up time, is it necessary?!
Assert(color[startnode] == RED)
#Assert(color[endnode] == YELLOW)

#MISSED CONDITION: No other first row cell reaches last row cell
for i1 in range(GRID_SIZE):
    for j1 in range(GRID_SIZE):
        for i2 in range(GRID_SIZE):
            for j2 in range(GRID_SIZE):
                if (not((i1==ENTRY_ROW and j1==ENTRY_COL) and (i2==EXIT_ROW and j2==EXIT_COL))):
                    Assert(Not(graph.reaches(nodes[0, i1, j1], nodes[GRID_SIZE-1, i2, j2])))

#5. Solve, and print out result!
result = Solve()
if result:
    print( "\ncolor" )
    print_color_map(ENTRY_ROW, ENTRY_COL, EXIT_ROW, EXIT_COL)
    print( "\n" )
    sys.stdout.flush()
else:
    print("UNSAT")
    sys.stdout.flush()

