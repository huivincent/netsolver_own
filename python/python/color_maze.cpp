#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <cstring>
#include <iostream>
//#include <bits/stdc++.h>
//#include "core/Config.h"
#include "core/Solver.h"
#include "simp/SimpSolver.h"
#include "graph/GraphTheory.h"
//#include "api/Monosat.h" //WRONG!
#include "bv/BVTheorySolver.h"
#include "bv/BVParser.h"

#include "utils/ParseUtils.h"
#include "utils/Options.h"
#include "pb/PbTheory.h"
#include "amo/AMOTheory.h"
#include "core/SolverTypes.h"
#include "mtl/Vec.h"

#define RED 0
#define GREEN 1
#define BLUE 2
#define YELLOW 3

typedef Monosat::Solver *  SolverPtr;
typedef Monosat::GraphTheorySolver<long> * GraphTheorySolver_long;
typedef Monosat::GraphTheorySolver<double>*  GraphTheorySolver_double;
typedef Monosat::BVTheorySolver<long>* BVTheoryPtr;

typedef int Node;
typedef int Edge;
typedef int BVID;

//using namespace Monosat;
using namespace std;

void vprint(string s, int verbose) {
    if (verbose > 0) {
        cout << s << endl;
    }
}

/*void print_color_map(BVID** maze, , int entrycol, int exitcol) { //color MAP?

}*/

int main () {
    int verbose = 2;
    int size, entrycol, exitcol, minlength; //TODO eventually make this argc, argv
    cin >> size >> entrycol >> exitcol >> minlength;

    assert(0<=entrycol && entrycol<=size-1);
    assert(0<=exitcol && exitcol<=size-1);
    assert(size<=minlength && minlength<=size*size);

    //SolverPtr S = newSolver(); can options be added this way?? TODO: what is this "MonosatData" struct??
    SolverPtr S = new Monosat::Solver();
    GraphTheorySolver_long graph = new Monosat::GraphTheorySolver<long>(S);
    S->addTheory(graph);

    BVTheoryPtr bv = new Monosat::BVTheorySolver<long>(S);
    graph->setBVTheory(bv);

    /*
    Node nodes[size][size];
    memset(nodes, 0, sizeof nodes);

    unordered_map<Node, BVID> color;
    vprint("Done initialize", verbose);

    //int bits[2][size*size]?? 
    for (int i=0; i<size; i++) {
        for (int j=0; j<size; j++) {
            nodes[i][j] = (Node) newNode(S, graph);
            int bits[2];    //is this garbage?? TODO: should we preallocate all?
            color[nodes[i][j]] = (BVID) newBitvector(S, bv, bits, 2);
        }
    }

    for (int i=0; i<size; i++) {
        for (int j=0; j<size; j++) {
            Node n = nodes[i][j];
            if (j < size-1)
            {
                Node other_n = nodes[i][j+1];
                Edge e = (Edge) newEdge(S, graph, (int)n, (int)other_n, 1);
                Edge e2 = (Edge) newEdge(S, graph, (int)other_n, (int)n, 1);
                //TODO: create an equals!!, AND function, AND over xnor of all bits
            }
            if (i < size-1)
            {
                
            }
            
        }
    }*/

    
    
    

    cout << "Hello World!" << endl;
    //delete(S);
    return 0;
}

/* Random tests:
 for (int i=0; i<size; i++) {
        for (int j=0; j<size; j++)
            cout << square[i][j] << " ";
        cout << endl;
    }
*/
