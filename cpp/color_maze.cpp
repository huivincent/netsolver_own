#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <cstring>
#include <iostream>
#include <unordered_map>
//#include "core/Config.h"
//#include "utils/Options.h"

//When you include files from other libraries, use <> notation, like this:
#include <monosat/core/Config.h>
#include <monosat/core/Solver.h>
#include <monosat/simp/SimpSolver.h>
#include <monosat/graph/GraphTheory.h>
#include <monosat/api/Circuit.h>
#include <monosat/bv/BVTheorySolver.h>
#include <monosat/bv/BVParser.h>
#include <monosat/core/SolverTypes.h>
#include <monosat/mtl/Vec.h>

//#include "api/Monosat.h" //WRONG!

using namespace Monosat;
using namespace std;

//Note: Pick whatever namespace you want for your project, but don't use Monosat or std.
//A namespace is the c++ analogue to a java package; it is similar conceptually, but instead of being defined by the
//directory structure, the namespace for each file is defined explicitly by wrapping it in a namespace{} construct:
namespace VincentProject {

class ColorMaze {

    ostringstream stringStream;

//prefer using 'const' to define constants, rather than macros.
//also, make sure they are defined in your namespace
    const int RED = 0;
    const int GREEN = 1;
    const int BLUE = 2;
    const int YELLOW = 3;

    typedef int Node; //Note: typedef does not actually 'define' a new type - it makes Node an alias for int
    //(they become the same type and can be used interchangeably)

    //typedef int Edge; //Edges are best represented as literals, not ints.
    typedef int BVID;

    void vprint(string s, int verbose) {
        if (verbose > 3) {
            cout << s << endl;
        }
    }


//Don't do this - it was a work around to support C in the Monosat.h api, but not something you should emulate elsewhere.
/*typedef SimpSolver *  SolverPtr;
typedef GraphTheorySolver<long> * GraphTheorySolver_long;
typedef GraphTheorySolver<double>*  GraphTheorySolver_double;
typedef BVTheorySolver<long>* BVTheorySolver<long> * ;*/

    SimpSolver S;
    Circuit<SimpSolver> c;
    
    BVTheorySolver<long> * bv=nullptr;
    GraphTheorySolver<long> * G = nullptr;

    int verbose = 3;
public:
    ColorMaze():c(S){
        //S.verbosity = 2;
        bv = new BVTheorySolver<long>(&S);
        G = new GraphTheorySolver<long>(&S);
        S.addTheory(G);
        G->setBVTheory(bv);//you only need this if you are using weighted edges
    }
    ~ColorMaze(){
    }

    void print_color_map(BVTheorySolver<long> *  bv, vec<vec<Node>>& nodes, unordered_map<Node, BVID>& color, int size, int entrycol,
                         int exitcol) { //color MAP?
        char colors[4] = {'R', 'G', 'B', 'Y'};
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                BVID colorBV = color[nodes[i][j]];
                int color = bv->getUnderApprox(colorBV); //width is 2?
                if (i == 0 && j == entrycol)
                    cout << 'S';
                else
                    cout << colors[color];
            }
            cout << endl;
        }
        cout << endl;
    }

    int newNode() {
        return G->newNode();
    }

    Lit newEdge(int from, int to, long weight) {
        Var v = S.newVar();
        Lit l = mkLit(v);
        G->newEdge(from, to, v, weight);
        return l;//Just return the lit directly //return toInt(l); //TODO:check?
    }

    int newBitvector(int *bits, int n_bits) {
        static vec<Var> lits;
        lits.clear();
        for (int i = 0; i < n_bits; i++) {
            lits.push(Var(bits[i]));
        }
        int bvID = bv->nBitvectors();
        stringStream << "BitVector assigned " << bvID;
        vprint(stringStream.str(), 0); stringStream.str("");
        bv->newBitvector(bvID, lits);
        stringStream << "BitVector# " << bvID << " allocated";
        vprint(stringStream.str(), 0); stringStream.str("");
        return bvID;
    }
    //replacing these with equivalent functions from api/Circuit.h
/*
    Lit And(SimpSolver * Lit a, Lit b) {
        Lit out = mkLit(S.newVar());
        S.addClause(a, ~out);
        S.addClause(b, ~out);
        S.addClause(~a, ~b, out);
        return out;
    }

    Lit Or(SimpSolver * Lit a, Lit b) {
        Lit out = mkLit(S.newVar());
        S.addClause(~a, out);
        S.addClause(~b, out);
        S.addClause(a, b, ~out);
        return out;
    }

    Lit Equal(SimpSolver * Lit a, Lit b) {
        //Out is XOR, ~Out XNOR
        Lit out = mkLit(S.newVar());
        S.addClause(a, b, out);
        S.addClause(~a, b, ~out);
        S.addClause(a, ~b, ~out);
        S.addClause(~a, ~b, out);
        return ~out;
    }*/

    Lit newBVComparison_const_equal(int bvID, long weight) {
        Var v = S.newVar();
        Var v2 = S.newVar();
        Lit l = mkLit(v);
        Lit l2 = mkLit(v2);
        bv->newComparison(Comparison::leq, bvID, weight, v);
        bv->newComparison(Comparison::geq, bvID, weight, v2);
        return c.And(l, l2);
    }

    Lit newBVComparison_bv_equal(int bvID, int compareID) {
        Var v = S.newVar();
        Var v2 = S.newVar();
        Lit l = mkLit(v);
        Lit l2 = mkLit(v2);
        bv->newComparisonBV(Comparison::leq, bvID, compareID, v);
        bv->newComparisonBV(Comparison::geq, bvID, compareID, v2);
        return c.And(l, l2);
    }

    Lit reaches(int from, int to) {
        Var v = S.newVar();
        Lit l = mkLit(v);
        G->reaches(from, to, v);
        G->implementConstraints();
        return l;
    }

    Lit shortestPath_leq_const(int from, int to, long dist) {
        Var v = S.newVar();
        Lit l = mkLit(v);
        G->distance(from, to, v, dist, true);
        G->implementConstraints();
        return l;
    }

/* int newBVComparison_const_geq(SimpSolver *  BVTheorySolver<long> * bv, int bvID, long weight){
	  Var v = S.newVar();
	  Lit l =mkLit(v);
	  bv->newComparison(Comparison::geq,bvID,weight,v);
	  return toInt(l);
 }
 int newBVComparison_bv_geq(SimpSolver *  BVTheorySolver<long> * bv, int bvID, int compareID){
	  Var v = S.newVar();
	  Lit l =mkLit(v);
	  bv->newComparisonBV(Comparison::geq,bvID,compareID,v);
	  return toInt(l);
 }*/
public:
    int generate(int size, int entrycol, int exitcol, int minlength) {


        //Node nodes[size][size];//It is usually a better idea to use std::vector or mtl/vec, which are like Java ArrayLists, rather than
        //c-style arrays.
        //memset/malloc/etc are from C. Usually, you should avoid them in c++.
        //memset(nodes, 0, sizeof nodes);

        //Minisat/Monosat vectors (<monosat/mtl/Vec.h>) are usually the right choice for arrays, including multidimensional arrays
        //when working with monosat (sometimes std::vector is also the right choice).
        //Technically, this is slightly less efficient than memset, but it usually won't matter, and has many other benefits

        vec<vec<Node>> nodes;
        nodes.growTo(size);
        for(int i = 0;i<nodes.size();i++){
            nodes[i].growTo(size);
        }

        unordered_map<Node, BVID> color;
        stringStream << "Done initialize";
        vprint(stringStream.str(), 3); stringStream.str("");

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                Node n = newNode();              
                nodes[i][j] = n;
                int bits[2]; bits[0]= S.newVar(); bits[1]= S.newVar();    
                //lifetime - keep reference?? TODO: should we preallocate all bits for this?          
                color[n] = newBitvector(bits, 2);
            }
        }

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                Node n = nodes[i][j];
                if (j < size - 1) {
                    Node other_n = nodes[i][j + 1];
                    Lit e = newEdge(n, other_n, 1); //you don't need to cast Node to int, since it _is_ an int (n, other_n, 1);
                    Lit e2 = newEdge(other_n, n, 1);
                    //TODO: create an equals!!, AND function, AND over xnor of all bits
                    //Assert neighbors have unequal color
                    stringStream << "Done Initializing Edge between " << n << " and " << other_n;
                    vprint(stringStream.str(), 3); stringStream.str("");

                    S.addClause(~newBVComparison_bv_equal(color[n], color[other_n]));
                    stringStream << "Asserted neighbors unequal " << i*size+j;
                    vprint(stringStream.str(), 3); stringStream.str("");

                    Lit b = c.Or(
                                          c.And( newBVComparison_const_equal(color[n], RED),
                                         newBVComparison_const_equal(color[other_n], GREEN)),
                                          c.Or(
                                                  c.And( newBVComparison_const_equal(color[n], GREEN),
                                            newBVComparison_const_equal(color[other_n], BLUE)),
                                                  c.Or(
                                                          c.And( newBVComparison_const_equal(color[n], BLUE),
                                               newBVComparison_const_equal(color[other_n], YELLOW)),
                                                          c.And( newBVComparison_const_equal(color[n], YELLOW),
                                               newBVComparison_const_equal(color[other_n], RED))
                                        )));
                    S.addClause(~e, b);
                    S.addClause(e, ~b);
                    Lit b2 = c.Or(
                                          c.And( newBVComparison_const_equal(color[other_n], RED),
                                         newBVComparison_const_equal(color[n], GREEN)),
                                          c.Or(
                                                  c.And( newBVComparison_const_equal(color[other_n], GREEN),
                                            newBVComparison_const_equal(color[n], BLUE)),
                                                  c.Or(
                                                          c.And( newBVComparison_const_equal(color[other_n], BLUE),
                                               newBVComparison_const_equal(color[n], YELLOW)),
                                                          c.And( newBVComparison_const_equal(color[other_n], YELLOW),
                                               newBVComparison_const_equal(color[n], RED))
                                        )));
                    S.addClause(~e2, b2);
                    S.addClause(e2, ~b2);
                    /*S.addClause(
                            c.Xnor(e,
                                  c.Or(
                                          c.And( newBVComparison_const_equal(color[n], RED),
                                         newBVComparison_const_equal(color[other_n], GREEN)),
                                          c.Or(
                                                  c.And( newBVComparison_const_equal(color[n], GREEN),
                                            newBVComparison_const_equal(color[other_n], BLUE)),
                                                  c.Or(
                                                          c.And( newBVComparison_const_equal(color[n], BLUE),
                                               newBVComparison_const_equal(color[other_n], YELLOW)),
                                                          c.And( newBVComparison_const_equal(color[n], YELLOW),
                                               newBVComparison_const_equal(color[other_n], RED))
                                        )))));

                    S.addClause(
                            c.Xnor(
                                  e2,
                                  c.Or(
                                          c.And( newBVComparison_const_equal(color[other_n], RED),
                                         newBVComparison_const_equal(color[n], GREEN)),
                                          c.Or(
                                                  c.And( newBVComparison_const_equal(color[other_n], GREEN),
                                            newBVComparison_const_equal(color[n], BLUE)),
                                                  c.Or(
                                                          c.And( newBVComparison_const_equal(color[other_n], BLUE),
                                               newBVComparison_const_equal(color[n], YELLOW)),
                                                          c.And( newBVComparison_const_equal(color[other_n], YELLOW),
                                               newBVComparison_const_equal(color[n], RED))
                                        )))));*/
                    stringStream << "Asserted 2 neighbor color patterns " << i*size+j;
                    vprint(stringStream.str(),3); stringStream.str("");

                }
                if (i < size - 1) {
                    Node other_n = nodes[i + 1][j];
                    Lit e = newEdge(  n, other_n, 1);
                    Lit e2 = newEdge(  other_n, n, 1);
                    //TODO: create an equals!!, AND function, AND over xnor of all bits
                    S.addClause(~newBVComparison_bv_equal(color[n], color[other_n]));

                    Lit b = c.Or(
                                          c.And( newBVComparison_const_equal(color[n], RED),
                                         newBVComparison_const_equal(color[other_n], GREEN)),
                                          c.Or(
                                                  c.And( newBVComparison_const_equal(color[n], GREEN),
                                            newBVComparison_const_equal(color[other_n], BLUE)),
                                                  c.Or(
                                                          c.And( newBVComparison_const_equal(color[n], BLUE),
                                               newBVComparison_const_equal(color[other_n], YELLOW)),
                                                          c.And( newBVComparison_const_equal(color[n], YELLOW),
                                               newBVComparison_const_equal(color[other_n], RED))
                                        )));
                    S.addClause(~e, b);
                    S.addClause(e, ~b);
                    Lit b2 = c.Or(
                                          c.And( newBVComparison_const_equal(color[other_n], RED),
                                         newBVComparison_const_equal(color[n], GREEN)),
                                          c.Or(
                                                  c.And( newBVComparison_const_equal(color[other_n], GREEN),
                                            newBVComparison_const_equal(color[n], BLUE)),
                                                  c.Or(
                                                          c.And( newBVComparison_const_equal(color[other_n], BLUE),
                                               newBVComparison_const_equal(color[n], YELLOW)),
                                                          c.And( newBVComparison_const_equal(color[other_n], YELLOW),
                                               newBVComparison_const_equal(color[n], RED))
                                        )));
                    S.addClause(~e2, b2);
                    S.addClause(e2, ~b2);

                    /*S.addClause(
                            c.Xnor(
                                  e,
                                  c.Or(
                                          c.And( newBVComparison_const_equal(color[n], RED),
                                         newBVComparison_const_equal(color[other_n], GREEN)),
                                          c.Or(
                                                  c.And( newBVComparison_const_equal(color[n], GREEN),
                                            newBVComparison_const_equal(color[other_n], BLUE)),
                                                  c.Or(
                                                          c.And( newBVComparison_const_equal(color[n], BLUE),
                                               newBVComparison_const_equal(color[other_n], YELLOW)),
                                                          c.And( newBVComparison_const_equal(color[n], YELLOW),
                                               newBVComparison_const_equal(color[other_n], RED))
                                        )))));

                    S.addClause(
                            c.Xnor(
                                  e2,
                                  c.Or(
                                          c.And( newBVComparison_const_equal(color[other_n], RED),
                                         newBVComparison_const_equal(color[n], GREEN)),
                                          c.Or(
                                                  c.And( newBVComparison_const_equal(color[other_n], GREEN),
                                            newBVComparison_const_equal(color[n], BLUE)),
                                                  c.Or(
                                                          c.And( newBVComparison_const_equal(color[other_n], BLUE),
                                               newBVComparison_const_equal(color[n], YELLOW)),
                                                          c.And( newBVComparison_const_equal(color[other_n], YELLOW),
                                               newBVComparison_const_equal(color[n], RED))
                                        )))));*/
                }
            }
        }

        vprint("Maze entry in col X and row 0", verbose);
        Node start = nodes[0][entrycol];
        S.addClause(newBVComparison_const_equal(color[start], RED));

        stringStream << "Asserted start is RED ";
        vprint(stringStream.str(), 3); stringStream.str("");

        Node end = nodes[size - 1][exitcol];
        vprint("Maze exit in col X and row 0", verbose);

        stringStream << "Reach constraint: start is " << start << ". End is " << end;
        vprint(stringStream.str(), 4); stringStream.str("");

        //Reaches constraint
        S.addClause(reaches( start, end));
        stringStream << "Asserted Reaches ";
        vprint(stringStream.str(), 3); stringStream.str("");

        //Distance constraint
        S.addClause(~shortestPath_leq_const(  start, end, minlength));
        stringStream << "Asserted MIN shortest path constraint ";
        vprint(stringStream.str(), 3); stringStream.str("");

        for (int col1 = 0; col1 < size; col1++) {
            for (int col2 = 0; col2 < size; col2++) {
                if (col1 != entrycol && col2 != exitcol) {
                    S.addClause(~reaches(  nodes[0][col1], nodes[size - 1][col2]));
                    stringStream << "Asserted unREACHES between " << nodes[0][col1] << " and " <<nodes[size - 1][col2] <<endl;
                    vprint(stringStream.str(), 4); stringStream.str("");
                }
            }
        }

        cout << "Solving!" << endl;

        //S.cancelUntil(0);
        S.preprocess();     //Note: Painful bug - Some theory vectors don't initialize if this not called.
        int result = S.solve();
        S.printStats(3);    //TODO: should make this dependent on "verb"

        if (result == 0) {
            cout << "UNSAT" << endl;
        }
        else if (result == 1) {
            cout << "SAT" << endl << endl;
            /*cout << "Printing out" << endl;*/
            print_color_map(bv, nodes, color, size, entrycol, exitcol);
        }
        else if (result == 2) {
            cout << "error" << endl;
        }

        return 0;
    }
};
};

//main method has to be outside of namespace
int main() {
    int verbose = 2;
    int size, entrycol, exitcol, minlength; //TODO eventually make this argc, argv??
    cin >> size >> entrycol >> exitcol >> minlength;

    assert(0 <= entrycol && entrycol <= size - 1);
    assert(0 <= exitcol && exitcol <= size - 1);
    assert(size <= minlength && minlength <= size * size);

    opt_verb = 2;
    opt_decide_theories = false;
    VincentProject::ColorMaze maze_generator;
    return maze_generator.generate(size,  entrycol,  exitcol,  minlength);
}
