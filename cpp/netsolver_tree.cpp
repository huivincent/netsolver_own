#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <cstring>
#include <iostream>
#include <vector>
#include <string>

#include <monosat/core/Config.h>
#include <monosat/core/Solver.h>
#include <monosat/simp/SimpSolver.h>
#include <monosat/graph/GraphTheory.h>
#include <monosat/api/Circuit.h>
#include <monosat/bv/BVTheorySolver.h>
#include <monosat/core/SolverTypes.h>
#include <monosat/mtl/Vec.h>

using namespace Monosat;
using namespace std;

namespace VincentProject {

typedef int BVID;

struct Edge {
	int to;
	int cap;
	vector<BVID> ids; //TODO does this alloc?
	Edge(int to, int cap): to(to), cap(cap) {}

	void addFlowBV(BVID id) { ids.push_back(id); }
};

struct VDemand {
	int from;
	int to;
	int bw;

	VDemand(int from, int to, int bw): from(from), to(to), bw(bw) {}
};

class NetSolverTree {
	ostringstream stringStream;

	SimpSolver S;
	Circuit<SimpSolver> c; //TODO, what does inner template mean?

	BVTheorySolver<long>* bv = nullptr;
	vector<GraphTheorySolver<long>* > graphs;

	public:
	NetSolverTree(): c(S){ //TODO, understand?
        //S.verbosity = 2;
        bv = new BVTheorySolver<long>(&S);
    }
    ~NetSolverTree(){
		for (GraphTheorySolver<long>* g: graphs)
			delete g;
    }

	//Usage: vprint("Reach constraint: start is " +to_string(start)+ ". End is " + to_string(end), 4); Example
	/*stringStream << "BitVector# " << bvID << " allocated"; vprint(stringStream.str(), 5); stringStream.str("");*/
	void vprint(string s, int verbose) {
        if (verbose > 3) {
            cout << s << endl;
        }
    }

	GraphTheorySolver<long>* newGraph() {
		GraphTheorySolver<long>* G = new GraphTheorySolver<long>(&S);
		S.addTheory(G);
		G->setBVTheory(bv);
		graphs.push_back(G);
		return G;
	}

    Lit newEdge(GraphTheorySolver<long>* G, int from, int to) {
        Var v = S.newVar();
        Lit l = mkLit(v);
        G->newEdge(from, to, v); //TODO check API
        return l;
    }

	Lit newEdgeBV(GraphTheorySolver<long>* G, int from, int to, BVID id) {
        Var v = S.newVar();
        Lit l = mkLit(v);
        G->newEdgeBV(from, to, v, id); //TODO check API
        return l;
    }

    Lit maxFlow_geq(GraphTheorySolver<long>* G, int source, int sink, long weight) {
        Var v = S.newVar();
        Lit l = mkLit(v);
        G->maxflow(source, sink, v, weight, true);
        G->implementConstraints();
        return l;
    }

    Lit maxFlow_gt(GraphTheorySolver<long>* G, int source, int sink, long weight) {
        Var v = S.newVar();
        Lit l = mkLit(v);
        G->maxflow(source, sink, v, weight, false);
        G->implementConstraints();
        return l;
    }

	Lit maxFlow_equal_const(GraphTheorySolver<long>* G, int source, int sink, long weight) {
        return c.And(maxFlow_geq(G,source,sink,weight), ~maxFlow_gt(G,source,sink,weight));
    }

    int newBitvector(int *bits, int n_bits) {
        static vec<Var> lits;
        lits.clear();
        for (int i = 0; i < n_bits; i++) {
            lits.push(Var(bits[i]));
        }
        int bvID = bv->nBitvectors();
        stringStream << "BitVector assigned " << bvID;
        vprint(stringStream.str(), 0); stringStream.str("");
        bv->newBitvector(bvID, lits);
        stringStream << "BitVector# " << bvID << " allocated";
        vprint(stringStream.str(), 0); stringStream.str("");
        return bvID;
    }

	void applyDemand(vector<vector<Edge> >& pgraph, vec<vec<Lit> >& VMassignment, vec<vec<int> >& VMdests, int from, int to, int bw) { //Ensure READ ONLY on first 2 args!
		//need to alloc BitVectors as int[]
		GraphTheorySolver<long>* comGraph = newGraph(); //create the commodity graph
		for (int i=0; i<pgraph.size(); i++) {
			for (int j=0; j<pgraph[i].size(); j++) {
				int* bits = new int[16]; //TODO: fix memory leak, allocate elsewhere?
				for (int i=0; i<16; i++) bits[i] = S.newVar();
				BVID id = newBitvector(bits, 16);
				newEdgeBV(comGraph, i, j, id);
				pgraph[i][j].addFlowBV(id);
			}
		}
		//MAXflow constraint
		for (int i=0; i<VMassignment[from].size(); i++) {
			for (int j=0; j<VMassignment[to].size(); j++) {
				Lit e = VMassignment[from][i];
				int assignedfrom = VMdests[from][i]; //Assumes VMdests/VMassignment sync'ed
				Lit e2 = VMassignment[to][j];
				int assignedto = VMdests[to][j];
				c.AssertImplies(c.And(e, e2), maxFlow_equal_const(comGraph, \
										assignedfrom, assignedto, bw) );
			}
		}
	}

	int solve() {
		int n, numedges, a, b, edge, cap;
		int m, demands, d1, d2, bandwidth;
		
		//1. INITIALIZATIONS		
		cin >> n >> numedges;
		edge = numedges;
		vector<vector<Edge> > ptopo(n, vector<Edge>());

		while (edge--) {
			cin >> a >> b >> cap; //assume edges directed
		    ptopo[a].push_back(Edge(b, cap) );
		}
		
		int numleaves, leaf;
		vector<int> leafnodes;
		cin >> numleaves;
		while(numleaves--) {
			cin >> leaf;
			leafnodes.push_back(leaf);
		}

		//save list of demands
		cin >> m >> demands;
		assert(m <= numleaves);
		vector<VDemand> demandlist;
		while (demands--) {
			cin >> d1 >> d2 >> bandwidth; //assume edges directed
		    demandlist.push_back(VDemand(d1, d2, bandwidth) );
		}

		/*for (int i=0; i<vtopo.size(); i++)
			for (int j=0; j<vtopo[i].size(); j++)
				vprint("Graph has edge from " + to_string(i) +" to "+ to_string(vtopo[i][j].to)+\
					", Cap: "+ to_string(vtopo[i][j].cap),5);*/

		//instantiate assignment graph!
		//Create symbolic edges: VM->PM
		GraphTheorySolver<long>* assignGraph = newGraph();
		vec<vec<Lit> > VMedges; VMedges.growTo(m);
		vec<vec<int> > VMdest; VMdest.growTo(m); //Inv: MUST sync by index w VMedges 
		vec<vec<Lit> > PMedges; PMedges.growTo(leafnodes.size());
		//TODO Assert ONE HOT constraints over all vec<Lit>; bijection on the set
		for (int i=0; i<m; i++) {
			for (int leaf: leafnodes) {
				Lit e = newEdge(assignGraph, i, leaf);
				VMedges[i].push(e);
				VMdest[i].push(leaf);
				PMedges[leaf].push(e);
			}
		}
		
		//iterate and apply demands
		//int bits[demandlist.size][]; //TODO refactor allocation?
		for (VDemand d: demandlist) {
			applyDemand(ptopo, VMedges, VMdest, d.from, d.to, d.bw);
		}

		//4. Assert BV sums are bounded appropriately - iterate through graph for this..
		

	}

};
};


int main() {
	VincentProject::NetSolverTree solvertree;
    solvertree.solve();
 
}
